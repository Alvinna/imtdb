let backgroundPort = browser.runtime.connect({name: "imtdb"});
let imdbId = getIMDbID();

function getIMDbID() {
  // Get movie/TV id.
  id = document.getElementById("quicklinksMainSection").querySelector(".quicklink").href.split("/")[4];
  return id;
}

function getTitle() {
  return document.querySelector("h1").textContent;
}

function viewMore() {
  // Fetch list and hide button.
  backgroundPort.postMessage({"type": "fetch_download_list", "data": {"imdbid": imdbId, "isall": true}});
  var seeMore = document.getElementById("seeMore");
  seeMore.style.display = "none";
  var filter = document.getElementById("downloadFilter");
  filter.style.display = "block";
}

function filterTable() {
  var text = document.getElementById("filterBox").value;
  backgroundPort.postMessage({"type": "fetch_filtered_download_list", "data": {"imdbid": imdbId, "filter": text}});
}

function inject() {

  // Create download section.
  var downloadSection = document.createElement("div");
  downloadSection.className = "article";
  // Create title.
  var title = document.createElement("h2");
  title.appendChild(document.createTextNode("Downloads"));
  // Create filter box
  var filter = document.createElement("div");
  filter.className = "imdb-header-search__input";
  filter.style.marginLeft = "5px";
  filter.style.marginTop = "5px";
  filter.style.display = "none";
  filter.id = "downloadFilter";
  var filterBox = document.createElement("input");
  filterBox.setAttribute("type", "text");
  filterBox.placeholder = "Filter"
  filterBox.style.height = "24";
  filterBox.id = "filterBox";
  var filterButton = document.createElement("button");
  filterButton.className = "btn";
  filterButton.appendChild(document.createTextNode("Search"))
  filterButton.id = "filterButton";
  filterButton.onclick = filterTable;
  filter.appendChild(filterBox);
  filter.appendChild(filterButton);
  // Create table.
  var table = document.createElement("table");
  var tableBody = document.createElement('tbody');
  tableBody.id = "tableBody";
  table.appendChild(tableBody);
  // Create see-more subsection.
  var seeMore = document.createElement("div");
  seeMore.className += "see-more";
  seeMore.id = "seeMore";
  var seeMoreText = document.createElement("button");
  seeMoreText.appendChild(document.createTextNode("See more"));
  seeMoreText.id = "seeMoreText";
  seeMoreText.onclick = viewMore;
  seeMoreText.style.background = "none";
  seeMoreText.style.border = "none";
  seeMoreText.style.padding = "0";
  seeMore.appendChild(seeMoreText);
  seeMore.appendChild(document.createTextNode("»"));
  // Add elements to section
  downloadSection.appendChild(title);
  table.appendChild(filter);
  downloadSection.appendChild(table);
  downloadSection.appendChild(seeMore);

  // Get the bottom section on the webpage.
  bottom = document.getElementById("main_bottom");
  // Add download section to the bottom section.
  bottom.insertBefore(downloadSection, bottom.firstChild);
  
  // Fetch download list.
  backgroundPort.postMessage({"type": "fetch_download_list", "data": {"imdbid": imdbId, "isall": false}});
}

function updateTable(data) {
  var tableBody = document.getElementById("tableBody");

  // Clear and style the table.
  tableBody.innerHTML = "";
  tableBody.style.display = "block";
  tableBody.style.marginRight = "0";
  tableBody.style.marginLeft = "0";
  tableBody.style.overflow = "auto";

  // Show nothing if no file found.
  if ( !data.hasOwnProperty("torrent_results")) {
    var seeMore = document.getElementById("seeMore");
    seeMore.style.display = "none";
    tableBody.appendChild(document.createTextNode("Nothing found!"));
    return;
  }
  
  var torrentList = data.torrent_results;
  // Hide view-more button if no more links to show.
  if (torrentList.length < 25) {
    var seeMore = document.getElementById("seeMore");
    seeMore.style.display = "none";
  }
  else {
    tableBody.style.height = "200px";
  }

  // Add download list to the table.
  var cnt;
  for (cnt = 0; cnt < torrentList.length; cnt++) {
    var tr = document.createElement("tr");
    tr.classList = (cnt & 1) ? "odd" : "even";
    var a = document.createElement("a");
    a.appendChild(document.createTextNode(torrentList[cnt].filename));
    a.href = torrentList[cnt].download;
    tr.appendChild(a);
    tableBody.appendChild(tr);
  }
}

backgroundPort.onMessage.addListener(messageHandler);

function messageHandler(message) {
  // Message should contain a type field and a data field
  switch(message.type) {
    case "return_download_list":
      updateTable(message.data);
      break;
  }
}

// Inject content to the webpage!
inject();