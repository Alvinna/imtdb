let torrentAPIUrl = "https://torrentapi.org/pubapi_v2.php";

function requestToken(appId, callback) {
    $.get(torrentAPIUrl + "?app_id=" + appId + "&get_token=get_token", callback);
}


function searchIMDbIdTop(appId, imdbid, callback) {

    function search(res) {
        setTimeout(() => { 
            $.get(torrentAPIUrl + "?app_id=" + appId + "&token=" + res.token + 
            "&mode=search&search_imdb=" + imdbid + "&ranked=0&limit=25", callback);
        }, 2000);
    }
    requestToken(appId, search);
}

function searchIMDbIdAll(appId, imdbid, callback) {
    function search(res) {
        setTimeout(() => {
            $.get(torrentAPIUrl + "?app_id=" + appId + "&token=" + res.token + 
            "&mode=search&search_imdb=" + imdbid + "&ranked=0&limit=100", callback);
        }, 2000);
    }
    requestToken(appId, search);
}

function searchIMDbIdFiltered(appId, imdbid, filter, callback) {
    function search(res) {
        setTimeout(() => {
            $.get(torrentAPIUrl + "?app_id=" + appId + "&token=" + res.token + 
            "&mode=search&search_imdb=" + imdbid + "&search_string=" + encodeURI(filter) + 
            "&ranked=0&limit=100", callback);
        }, 2000);
    }
    requestToken(appId, search);
}