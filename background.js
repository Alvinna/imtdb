// Store content script connection.
let contentPort;
let appid = "imtdb";

// Handle connection.
function connected(port) {
  contentPort = port;
  contentPort.onMessage.addListener(messageHandler);
}

browser.runtime.onConnect.addListener(connected);

// Handle message.
function messageHandler(message) {
  // Message should contain a type filed and a data field
  switch(message.type) {
    case "fetch_download_list":
      fetchDownloadList(message.data.imdbid, message.data.isall);
      break;
    case "fetch_filtered_download_list":
      fetchFilteredDownloadList(message.data.imdbid, message.data.filter);
      break;
  }
}

// Fetch download list
function fetchDownloadList(imdbid, isAll) {
  if (isAll) {
    searchIMDbIdAll("imtdb", imdbid, returnResults);
  }
  else {
    searchIMDbIdTop("imtdb", imdbid, returnResults);
  }
}

function fetchFilteredDownloadList(imdbid, filter) {
  searchIMDbIdFiltered("imtdb", imdbid, filter, returnResults);
}

function returnResults(data) {
  console.log(data);
  contentPort.postMessage({"type": "return_download_list", "data": data});
}
